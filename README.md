how to start:

start virtual env

Git clone:
git clone https://danilovmy@bitbucket.org/danilovmy/python_conferences.git

pip install requirements

python manage.py makemigrations
python manage.py migrate
md media
python manage.py createsuperuser
python manage.py runserver

goto localhost:8000/ru/admin
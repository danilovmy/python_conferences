from django.views.generic import TemplateView
from .models import Category

class MainView(TemplateView):
    template_name = 'core/index.html'

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['opts'] = Category._meta
        return context

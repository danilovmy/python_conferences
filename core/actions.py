from django.core.cache.backends.locmem import OrderedDict
# Create your admin actions here.

from django.contrib import messages
from django.contrib.admin.helpers import ACTION_CHECKBOX_NAME
from django.contrib.admin.models import LogEntry, ADDITION, CHANGE, DELETION
from django.contrib.admin.options import get_content_type_for_model
from django.core.exceptions import ImproperlyConfigured
from django.template import TemplateDoesNotExist
from django.template.loader import select_template
from django.utils.decorators import classonlymethod
from django.utils.encoding import force_text
from django.utils.functional import cached_property
from django.views.generic import FormView
from django.utils.translation import ugettext_lazy as _

from core.forms import YesNoForm


class ActionView(FormView):
    ADDITION = ADDITION
    CHANGE = CHANGE
    DELETION = DELETION
    logger = LogEntry
    template_name = 'core/confirmation.html'
    template_name_suffix = 'confirmation'
    callback_template = None
    callback_template_name_suffix = 'callback'
    allow_empty = False
    short_description = _('The Name is not defined')
    description = ''
    counter = 0
    cancel_message = _('Action was cancelled')
    finished_message = ''
    processing_message = _('{counter} was change')
    process_message = _('{obj} was change')
    extra_context = {'action_checkbox_name': ACTION_CHECKBOX_NAME}
    model = None
    admin = None
    queryset = None
    request = None
    permissions = ()
    permissions_message_error = _('You don\'t have enough permissions')

    @cached_property
    def answer(self):
        return YesNoForm(**super(ActionView, self).get_form_kwargs())

    @classmethod
    def name(cls):
        return cls.__name__

    def post(self, request, *args, **kwargs):
        if self.answer.is_valid():
            if self.answer.is_positiv:
                if self.get_form_class():  # answer is Positiv, processing with or without form
                    return super(ActionView, self).post(request, *args, **kwargs)  # form processing
                return self.processing(*args, **kwargs)  # processing without form
            return self.finished(cancel=True)  # answer is Negativ
        return self.get(request, *args, **kwargs)  # answer is not valid

    def get(self, request, *args, **kwargs):
        if self.get_form_class():
            self.request.method = 'GET'
            return super(ActionView, self).get(request, *args, **kwargs)
        return self.render_to_response(self.get_context_data(**kwargs))

    def form_valid(self, *args, **kwargs):
        """Make something ready befor processing."""
        return self.processing(*args, **kwargs)

    def processing(self, *args, **kwargs):
        """Make main process."""
        for obj in self.get_queryset():
            if self.process(obj):
                self.counter += 1
                self.info(self.process_message, obj=obj)
                self.log(obj)
        if self.counter:
            kwargs['message'] = self.processing_message
        return self.finished(*args, **kwargs)

    def process(self, obj, *args, **kwargs):
        """override this"""
        raise NotImplementedError(_('It should be overridden'))
        return True or False


    def finished(self, *__, **kwargs):
        """Send message after process and return None."""
        message = kwargs.get('cancel') and self.cancel_message or kwargs.get('message') or self.finished_message
        if message:
            self.info(message, counter=self.counter, **kwargs)

    def get_form(self, form_class, **kwargs):
        form = super(ActionView, self).get_form(form_class)
        if hasattr(form, 'prepare'):
            return form.prepare(request=self.request, **dict({'prefix': self.get_prefix()}, **kwargs))
        form.request = self.request
        return form

    def get_prefix(self):
        return getattr(self, 'prefix', None)

    @classonlymethod
    def as_view(cls, **initkwargs):
        response = super(ActionView, cls).as_view(**initkwargs)
        response.short_description = cls.short_description
        response.action_class = cls
        return response

    def dispatch(self, admin, request, queryset, *args, **kwargs):
        vars(self).update(admin=admin, model=admin.model, queryset=queryset, request=request)
        if self.check_permissions(request, admin):
            return super(ActionView, self).dispatch(request, *args, **kwargs)
        return self.finished(message=self.permissions_message_error)

    @classmethod
    def check_permissions(cls, request, admin=None):
        return request.user.is_superuser or not getattr(cls, 'permissions', None) or (
            request.user.has_perms(permission.format(model_name=admin.model._meta.model_name, app_label=admin.model._meta.app_label.lower()) for permission in cls.permissions))

    @classmethod
    def patch_request_data(cls, data):
        if cls.allow_empty and not data.getlist(ACTION_CHECKBOX_NAME):
            data = data.copy()
            data[ACTION_CHECKBOX_NAME] = '-1'
            data._mutable = False
        return data

    @classmethod
    def get_as_tuple(cls):
        return cls.as_view(), cls.name(), cls.short_description

    def get_queryset(self, **__):
        if self.queryset is None:
            if self.model:
                self.queryset = self.model.objects.none()
            elif self.admin:
                self.queryset = self.admin.queryset(self.request).none()
        return self.queryset

    def get_object(self):
        for obj in self.get_queryset():
            return obj

    def log(self, obj, action_flag=CHANGE, **kwargs):
        LogEntry.objects.log_action(
            user_id=self.request.user.pk,
            content_type_id=get_content_type_for_model(obj).pk,
            object_id=obj.pk,
            object_repr=str(obj),
            action_flag=action_flag,
            change_message=kwargs.get('change_message') or self.processing_message,
        )

    def info(self, info, **kwargs):
        info = str(info.format(**kwargs))
        getattr(messages, kwargs.get('message_type') or 'info', messages.info)(self.request, info)
        return info

    def error(self, error, **kwargs):
        return self.info(error, message_type='error', **kwargs)

    def warning(self, warning, **kwargs):
        return self.info(warning, message_type='warning', **kwargs)

    def get_template_names(self):
        try:
            return super(ActionView, self).get_template_names()
        except ImproperlyConfigured:
            opts = self.model._meta
            return ['admin/{}/{}/{}_{}.html'.format(opts.app_label, opts.object_name, self.name(), self.callback_template_name_suffix).lower(),
                    'admin/{}/{}_{}.html'.format(opts.app_label, self.name(), self.template_name_suffix).lower(),
                    'admin/{}_{}.html'.format(self.name(), self.template_name_suffix).lower(),
                    'admin/{}.html'.format(self.template_name_suffix).lower()]

    def get_callback_template(self):
        self.template_name = self.callback_template
        self.template_name_suffix = self.callback_template_name_suffix

        templates = self.get_template_names()

        delattr(self, 'template_name')
        delattr(self, 'template_name_suffix')

        try:
            return select_template(templates).name
        except TemplateDoesNotExist:
            return ''

    def get_context_data(self, **kwargs):
        opts = self.model._meta  # pylint: disable=protected-access
        context = {
            'title': '%s. %s' % (str(self.short_description), str(_(u'Are you sure?'))),
            'action_short_description': self.short_description,
            'action_description': self.description,
            'action_name': self.name(),
            'objects_name': force_text(opts.verbose_name_plural),
            'action_objects': [self.get_queryset()],
            'callback_template': self.get_callback_template(),
            'queryset': self.get_queryset(),
            'opts': opts,
            'app_label': opts.app_label,
            'answer_form': self.answer,
        }
        if not self.get_form_class():
            context['form'] = ''
        context.update(kwargs)
        context.update(self.extra_context)
        return super(ActionView, self).get_context_data(**context)



class ActionViewsMixin(object):

    def changelist_view(self, request, extra_context=None):
        data = (request.method == 'POST') and request.POST
        if data and not data.get('_save') and data.get('action') and not data.getlist(ACTION_CHECKBOX_NAME):
            action = self.get_actions(request).get(data['action'])
            if action and getattr(action[0], 'action_class', None):
                request.POST = action[0].action_class.patch_request_data(data)
        return super(ActionViewsMixin, self).changelist_view(request, extra_context)

    def get_actions(self, request):
        return OrderedDict((key, action) for key, action in super(ActionViewsMixin, self).get_actions(request).items() if not hasattr(action[0], 'action_class') or action[0].action_class.check_permissions(request, self))


class MyOtherActionView(ActionView):

    def process(self, obj):
        print(obj)
        return obj
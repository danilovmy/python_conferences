from importlib import reload, import_module
from django.urls.base import clear_url_caches
from .actions import ActionView
from django.template.loader import get_template

from django.contrib.admin import ModelAdmin, TabularInline, StackedInline
from django.contrib.admin.sites import site
from django.conf import settings
from django.contrib.auth.models import Permission
from django.contrib.admin.sites import site
from django.utils.translation import ugettext_lazy as _

from django.contrib.admin.widgets import RelatedFieldWidgetWrapper
from .models import Product, Image, Store, models, Category, UNKNOWN


SET_RELATED_ELEMS_OFF = {'can_delete_related': False, 'can_add_related': False, 'can_change_related': False}

class ImageAdminInline(TabularInline):
    model = Image
    extra = 0

class ProductAdmin(ModelAdmin):
    inlines = ImageAdminInline,
    fields = 'name', 'description',

    def image_inline(self, obj=None):
        return

class ProductInline(StackedInline):
    model = Product
    fields = 'name', 'description',
    extra = 1


class ProxyModelAdmin(ProductAdmin):

    def get_queryset(self, *args, **kwargs):
        return super().get_queryset(*args, **kwargs)


class CategoryAdmin(ModelAdmin):
    save_on_top = True
    actions = ActionView.as_view(),

    def save_model(self, request, obj, form, change):
        super().save_model(request, obj, form, change)

site.register(Category, CategoryAdmin)

#reload(import_module(settings.ROOT_URLCONF))
#clear_url_caches()
class StoreAdmin(ModelAdmin):
    inlines = ProductInline,
    save_on_top = True

import json
from io import BytesIO, FileIO

from django.contrib.admin import ModelAdmin
from django.contrib.messages import get_messages
from django.contrib.messages.storage.fallback import FallbackStorage
from django.core.files.uploadedfile import SimpleUploadedFile
from django.db.models import Model, UniqueConstraint
from django.forms import forms
from django.template.response import TemplateResponse
from django.test import TestCase, RequestFactory
from django.contrib.auth.models import AnonymousUser

from core.actions import ActionView
from core.forms import YesNoForm
from users.models import User


class TestActionView(TestCase):

    @classmethod
    def setUpTestData(cls):
        cls.admin = ModelAdmin(model=User, admin_site='test')
        cls.user = User.objects.create_superuser(username='super', password='xxx')
        super().setUpTestData()

    def setUp(self) -> None:
        self.request = RequestFactory().request()
        setattr(self.request, 'session', 'session')
        self.messages = FallbackStorage(self.request)
        setattr(self.request, '_messages', self.messages)
        self.request.POST._mutable = True
        self.request.method = 'POST'
        self.action = ActionView(request=self.request, model=User, finished_message='success', admin=self.admin)

    def test_answer(self):
        self.assertIsInstance(self.action.answer, YesNoForm)

    def test_dispatch(self):
        self.action.dispatch(self.admin, self.request, 'queryset')
        self.assertIsInstance(self.action.admin, self.admin.__class__)
        self.assertIs(self.action.model, self.admin.model)
        self.assertEqual(self.action.queryset, 'queryset')

    def test_info(self):
        self.assertEqual(self.action.info('info'), 'info')
        self.assertEqual(str(get_messages(self.request)._queued_messages[-1].level), '20')

    def test_error(self):
        self.assertEqual(self.action.error('error'), 'error')
        self.assertEqual(str(get_messages(self.request)._queued_messages[-1].level), '40')

    def test_warning(self):
        self.assertEqual(self.action.warning('warning'), 'warning')
        self.assertEqual(str(get_messages(self.request)._queued_messages[-1].level), '30')

    def test_logger(self):
        def raise_exc(msg):
            raise Exception(msg)
        self.action.admin.log_change = lambda *args, **kwargs: raise_exc('change')
        self.action.admin.log_addition = lambda *args, **kwargs: raise_exc('add')
        self.action.admin.log_deletion = lambda *args, **kwargs: raise_exc('delete')
        self.assertRaisesMessage(Exception, 'change', self.action.log, object())
        self.assertRaisesMessage(Exception, 'change', self.action.log, object(), action='test')
        self.assertRaisesMessage(Exception, 'change', self.action.log, object(), action='change')
        self.assertRaisesMessage(Exception, 'add', self.action.log, object(), action='addition')
        self.assertRaisesMessage(Exception, 'delete', self.action.log, object(), action='deletion')

    def test_check_permissions(self):
        test_view = ActionView()
        with self.assertRaises(TypeError) as error:
            test_view.check_permissions()  # pylint: disable=no-value-for-parameter
        self.assertIn('\'permission\'', str(error.exception))

        self.assertIsNone(test_view.check_permissions('change', fall_silently=True))

        self.request.user = User(is_superuser=True, is_active=True, username='superuser')
        test_view.request = self.request
        test_view.model = User
        self.assertTrue(test_view.check_permissions('change', fall_silently=True))

        self.request.user.is_active = False
        self.assertFalse(test_view.check_permissions('change', fall_silently=True))

        self.request.user = AnonymousUser()
        self.assertFalse(test_view.check_permissions('change', fall_silently=True))

        self.request.user = User(is_superuser=False, is_active=True, username='normal_user')
        self.request.user.has_perm = lambda perm, *args, **kwargs: perm == 'users.change_user'
        self.assertTrue(test_view.check_permissions('change', fall_silently=True))
        self.assertFalse(test_view.check_permissions('add', fall_silently=True))

        test_view.error = lambda message, *args, **kwargs: (_ for _ in ()).throw(Exception(message))
        with self.assertRaises(Exception) as error:
            test_view.check_permissions('add')
        self.assertEqual(str(error.exception), str(test_view.permission_error_message))

    def test_get_queryset(self):
        self.assertIsNone(ActionView().get_queryset())
        sentinel = object()
        self.assertEqual(ActionView(queryset=sentinel).get_queryset(), sentinel)

        model = type('my_user', (User,), {'objects': sentinel, '__module__': User.__module__})
        self.assertEqual(ActionView(model=model).get_queryset(), sentinel)

        admin = type('my_user_admin', (ModelAdmin,), {'get_queryset': lambda *args, **kwargs: sentinel})
        self.assertEqual(ActionView(admin=admin(model=User, admin_site='test')).get_queryset(), sentinel)


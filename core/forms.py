from django import forms
from django.utils.functional import cached_property
from django.utils.safestring import mark_safe
from django.utils.translation import ugettext_lazy as _



class YesNoMixIn(object):
    POSITIVE = 'yes'
    NEGATIVE = 'no'
    POSITIVE_TEXT = _('Yes, I\'m sure')
    NEGATIVE_TEXT = _('No, I\'m not sure')
    CHOICES = ((POSITIVE, POSITIVE_TEXT), (NEGATIVE, NEGATIVE_TEXT))
    button_template = '<button type="{submit}" name="answer" value="{value}">{title}</button>'.format
    answer_help_text = answer_label = ''
    method = 'POST'
    process_url = ''
    form_template = 'core/extraform.html'

    def get_cleaned_data(self, *args, **kwargs):
        return self.cleaned_data if self.is_valid() else {}

    def get_cleaned_field(self, field=None, **kwargs):
        return self.get_cleaned_data().get(self.get_field_name(field))

    def get_field_name(self, field=None, **kwargs):
        return field or getattr(self, 'field', None) or list(self.fields.keys())[0]

    def get_request(self):
        return vars(self).get('request')

    def prepare(self, *args, **kwargs):
        for key, val in kwargs.items():
            setattr(self, key, val)
        self.args = args
        answer = self.fields['answer']
        answer.choices = [self._get_choice('POSITIVE'), self._get_choice('NEGATIVE')]
        answer.label = self.answer_label
        answer.help_text = self.answer_help_text
        return self

    @cached_property
    def is_positiv(self):
        return self.get_cleaned_field() == self.POSITIVE

    def _get_choice(self, choice):
        return getattr(self, choice), getattr(self, '{}_TEXT'.format(choice))

    def _render_button(self, value, text):
        return mark_safe(self.button_template(submit='submit', name=self.add_prefix('answer'), value=value, title=str(text)))

    def render_button_positive(self):
        return self._render_button(*self._get_choice('POSITIVE'))

    def render_button_negative(self):
        return self._render_button(*self._get_choice('NEGATIVE'))


class YesNoForm(YesNoMixIn, forms.Form):

    prefix = 'YesNoForm'
    answer = forms.ChoiceField(choices=YesNoMixIn.CHOICES, widget=forms.RadioSelect())
    button_template = '<button type="{submit}" name="{name}" onclick="document.getElementById(`layer`).style.display = `block`;" value="{value}">{title}</button>'.format

from django.urls import path
from django.contrib.admin.sites import site

from django.conf.urls.i18n import i18n_patterns

from core.views import MainView

urlpatterns = [path('admin/', site.urls),]

urlpatterns += [
    path('', MainView.as_view(), name='index')
]

urlpatterns = i18n_patterns(*urlpatterns)

try:
    from .local_urls import urlpatterns as local_patterns
    urlpatterns += local_patterns
except ImportError:
    pass

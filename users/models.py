from django.utils.translation import ugettext_lazy as _
from django.contrib.auth.models import AbstractUser

class User(AbstractUser):

    class Meta:
        verbose_name = _('My User Model')
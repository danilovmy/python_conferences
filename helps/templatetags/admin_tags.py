from django.contrib.admin.templatetags.admin_list import register as admin_list
from django.contrib.admin.templatetags.admin_list import change_list_object_tools_tag
from django.utils.translation import ugettext_lazy as _
from django.contrib.admin.templatetags.admin_modify import register as admin_modify, change_form_object_tools_tag


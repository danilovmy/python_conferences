# Create your admin actions here.
from json import loads
from django.contrib import messages
from django.contrib.admin.helpers import ACTION_CHECKBOX_NAME

from django.utils.decorators import classonlymethod
from django.views.generic import TemplateView
from django.utils.translation import ugettext_lazy as _


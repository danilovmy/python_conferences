from django.utils.translation import ugettext_lazy as _
from django.contrib.admin.options  import ModelAdmin

from django.contrib.admin.sites import site
from .models import HelpArticle
# from .actions import MyActionView

class HelpArticleAdmin(ModelAdmin):
    pass

site.register(HelpArticle, HelpArticleAdmin)